import { ToastService } from './../../services/toast.service';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { DataService } from './../../services/data.service';
import { Component, OnInit, Inject } from '@angular/core';
import { config } from 'src/app/services/config';
import {
  fadeInOnEnterAnimation,
  fadeOutOnLeaveAnimation,
  fadeInUpOnEnterAnimation,
  bounceInUpOnEnterAnimation,
} from 'angular-animations';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
@Component({
  templateUrl: './excel-download.component.html',
  styleUrls: ['./excel-download.component.scss'],
  animations: [
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
    fadeInUpOnEnterAnimation(),
    bounceInUpOnEnterAnimation(),
  ],
})
export class ExcelDownloadComponent implements OnInit {
  downloadUrl = '';
  showLoad = true;
  nomeArquivo = '';
  constructor(
    private _bottomSheetRef: MatBottomSheetRef<ExcelDownloadComponent>,
    private dataSrv: DataService,
    private toastSrv: ToastService,
    @Inject(MAT_BOTTOM_SHEET_DATA)
    public excelPath: {
      comeFrom: string;
      data: any;
    }
  ) {}
  ngOnInit(): void {
    console.log(this.excelPath);

    switch (this.excelPath.comeFrom) {
      case 'saldoConta':
        this.downloadSaldoContaExcel();
        break;
      case 'extrato':
        this.downloadExtratoExcel();
        break;
      case 'totalConta':
        this.downloadTotalContaExcel();
        break;
    }
  }

  downloadExtratoExcel() {
    this.dataSrv.gerarExcelExtratos.subscribe(
      (res: any) => {
        const excelObj = res.tab1;
        console.log('Linha 52', excelObj[0]);
        this.nomeArquivo = excelObj[0].nomeArquivo;
        this.downloadUrl = config.api.downloadExcel(excelObj[0].nomeArquivo);
        this.toastSrv.showToastSucess(
          'Arquivo ' + this.nomeArquivo + ' pronto para download'
        );
        this.showLoad = false;
      },
      (err) => {
        this.toastSrv.showToastError(
          'Houve um erro ao tentar gerar seu arquivo.',
          'Tente novamente mais tarde'
        );
        this._bottomSheetRef.dismiss();
      }
    );
  }
  downloadSaldoContaExcel() {
    this.dataSrv.gerarExcelSaldoConta.subscribe(
      (res: any) => {
        const excelObj = res.tab1;
        console.log('Linha 64', excelObj[0]);
        this.nomeArquivo = excelObj[0].nomeArquivo;
        this.downloadUrl = config.api.downloadExcel(this.nomeArquivo);
        this.toastSrv.showToastSucess(
          'Arquivo ' + this.nomeArquivo + ' pronto para download'
        );
        this.showLoad = false;
      },
      (err) => {
        this.toastSrv.showToastError(
          'Houve um erro ao tentar gerar seu arquivo.',
          'Tente novamente mais tarde'
        );
        this._bottomSheetRef.dismiss();
      }
    );
  }
  downloadTotalContaExcel() {
    this.dataSrv.gerarExcelTotalSaldoConta.subscribe(
      (res: any) => {
        const excelObj =res.tab1;
        console.log('Linha 64', excelObj[0]);
        this.nomeArquivo = excelObj[0].nomeArquivo;
        this.downloadUrl = config.api.downloadExcel(this.nomeArquivo);
        this.toastSrv.showToastSucess(
          'Arquivo ' + this.nomeArquivo + ' pronto para download'
        );
        this.showLoad = false;
      },
      (err) => {
        this.toastSrv.showToastError(
          'Houve um erro ao tentar gerar seu arquivo.',
          'Tente novamente mais tarde'
        );
      }
    );
  }
  close() {
    this._bottomSheetRef.dismiss();
  }
}
