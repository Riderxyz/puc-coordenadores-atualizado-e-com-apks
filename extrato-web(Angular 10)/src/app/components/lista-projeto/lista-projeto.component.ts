import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ProjListInterface } from './../../models/projList.interface';
import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
@Component({
  templateUrl: './lista-projeto.component.html',
  styleUrls: ['./lista-projeto.component.scss'],
})
export class ListaProjetoComponent implements OnInit {
  projList: ProjListInterface[] = [];
  constructor(
    private dataSrv: DataService,
    private dialog: MatDialogRef<ListaProjetoComponent>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.dataSrv.getUserProjList.subscribe((proj: any) => {
      console.log(proj);
      this.projList = proj.tabProjetos;
    });
  }

  onProjSelection(proj: ProjListInterface) {
    console.log(proj);
    this.dialog.beforeClosed().subscribe(() => {
      this.dataSrv.selectedProj = proj;
      // console.log('Teste', moment(this.dataSrv.dataInicio).format('L'));
      this.router.navigateByUrl('/extrato');
    });
    this.dialog.close();
  }
}
