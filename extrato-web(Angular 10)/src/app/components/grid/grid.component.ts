import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import {
  GridOptions,
  GridApi,
  ColumnApi,
  ColDef,
  RowDoubleClickedEvent,
  SelectionChangedEvent
} from "ag-grid-community";
import { config } from "../../services/config";

@Component({
  selector: "app-grid",
  templateUrl: "./grid.component.html",
  styleUrls: ["./grid.component.scss"]
})
export class GridComponent implements OnInit, OnChanges, OnDestroy {
  public gridApi: GridApi;
  public gridOptions: GridOptions;
    @Input() gridData = [];
  @Input() columnDefs: ColDef[] = [];
  @Input() colunasFiltraveis: string[];
  @Input() filtro = "";
  @Input() width = "100%";
  @Input() autoResize = false;
  @Input() height = "calc(60vh - 30px)";
  @Output() GridDobleClick = new EventEmitter<any>();
  @Output() GridSelectedRow = new EventEmitter<any>();
  @Output() GridReady = new EventEmitter<any>();
  isGridReady = false;
  allowEdit = false;
  gridDimensions = {};
  selectedRows = [];
  constructor(
    ) {}

  ngOnInit() {
    this.gridDimensions = {
      height: this.height,
      width: this.width
    };
    this.startGrid();
    if (this.colunasFiltraveis === undefined) {
      this.colunasFiltraveis = [];
      this.columnDefs.forEach(element => {
        this.colunasFiltraveis.push(element.field);
      });
    }
    window.onresize = resizeObj => {
      if (this.autoResize) {
        console.log('Olaaaaa');
        this.gridApi.sizeColumnsToFit();
      }
    };
  }
  ngOnChanges(changes) {
    if (changes.height) {
   //   console.log(changes);
    }
    if (changes.gridData !== undefined) {
      if (changes.gridData.previousValue !== undefined) {
        this.gridOptions.api.setRowData(this.gridData);
        if (this.autoResize) {
        this.gridOptions.api.sizeColumnsToFit();
      }
    }
    }
    if (changes.filtro !== undefined) {
      if (changes.filtro.previousValue !== undefined) {
        if (this.gridApi !== undefined) {
          this.gridApi.onFilterChanged();
        }
      }
    }
  }

  startGrid() {
    this.gridOptions = {
      columnDefs: this.columnDefs,
      defaultColDef: {
        editable: false,
        sortable: true,
        resizable: true,
        filter: true
      },
      suppressHorizontalScroll: false,
      overlayNoRowsTemplate: `
      <span class='ag-overlay-loading-center'>Sem dados para exibir</span>
      `,
      overlayLoadingTemplate: `<span'>carregando dados</span>`,
      suppressPaginationPanel: true,
      rowSelection: "multiple",
      onSelectionChanged: params => {
        this.onCheckBoxSelection(params);
      },
      onRowDoubleClicked: params => {
        this.onDoubleClick(params);
      },
      rowHeight: 100,
      isExternalFilterPresent: this.externalFilterPresent.bind(this),
      doesExternalFilterPass: this.externalFilterPass.bind(this),
      onGridReady: params => {
        this.gridApi = params.api;
        if (this.autoResize) {
        this.gridApi.sizeColumnsToFit();
        }
        this.GridReady.emit(this.gridApi);
      }
    };
  }
  onCheckBoxSelection(params: SelectionChangedEvent) {
    this.selectedRows = this.gridApi.getSelectedRows();
    console.log("dados da linha", this.selectedRows);
    console.log(this.selectedRows.length === 1);
    this.GridSelectedRow.emit(this.gridApi.getSelectedRows());

    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }
  onDoubleClick(ev: RowDoubleClickedEvent) {
    console.log("quantos foram selecionandos", ev);
    this.GridDobleClick.emit(ev.data);
  }

  externalFilterPresent() {
    return this.filtro !== "";
  }
  externalFilterPass(node: any) {
    let retorno = false;
    this.colunasFiltraveis.forEach(element => {
      if (this.filtro !== "") {
        if (
          (node.data[element] + "")
            .toLowerCase()
            .indexOf(this.filtro.toLowerCase()) !== -1
        ) {
          retorno = true;
        }
      } else {
        retorno = false;
      }
    });
    return retorno;
  }

  ngOnDestroy(): void {
    this.gridApi.resetQuickFilter();
    this.gridApi.onFilterChanged();
  }
}
