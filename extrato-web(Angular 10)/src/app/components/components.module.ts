import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoaderComponent } from './loader/loader.component';
import { GridComponent } from './grid/grid.component';
import { MaterialModule } from './../material.module';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaProjetoComponent } from './lista-projeto/lista-projeto.component';
import { AgGridModule } from 'ag-grid-angular';
import { ExcelDownloadComponent } from './excel-download/excel-download.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
const form = [FormsModule, ReactiveFormsModule];

@NgModule({
  declarations: [
    ListaProjetoComponent,
    HeaderComponent,
    GridComponent,
    ExcelDownloadComponent,
    LoaderComponent,
    EditarUsuarioComponent,
  ],
  imports: [CommonModule, ...form, MaterialModule, AgGridModule.withComponents([])],
  exports: [
    ListaProjetoComponent,
    HeaderComponent,
    GridComponent,
    LoaderComponent,
  ],
  providers: [],
})
export class ComponentsModule {}
