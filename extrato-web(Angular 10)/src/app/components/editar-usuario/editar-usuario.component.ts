import { MatDialogRef } from '@angular/material/dialog';
import { UserDataInterface } from './../../models/userData.interface';
import { DataService } from './../../services/data.service';
import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.scss'],
})
export class EditarUsuarioComponent implements OnInit {
  userObj: UserDataInterface;
  constructor(
    private dataSrv: DataService,
    private loginSrv: LoginService,
    public dialog: MatDialogRef<EditarUsuarioComponent>
  ) {}

  ngOnInit(): void {
    this.userObj = this.dataSrv.LoggedUserData;
  }

  onSubmit(event: NgForm) {
    console.log('Form Emitido');
    this.loginSrv.editUserData(this.userObj);
    this.dialog.close();
  }
}
