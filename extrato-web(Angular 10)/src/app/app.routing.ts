import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExtratosComponent } from './pages/extratos/extratos.component';
import { SaldoContasComponent } from './pages/saldo-contas/saldo-contas.component';
import { TotalContasComponent } from './pages/total-contas/total-contas.component';


const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
  path:'login',
  component: LoginComponent,
},
{
  path:'home',
  component: HomeComponent,
},
{
  path:'extrato',
  component: ExtratosComponent,
},
{
  path:'saldoConta',
  component: SaldoContasComponent
},
{
  path:'totalConta',
  component: TotalContasComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
