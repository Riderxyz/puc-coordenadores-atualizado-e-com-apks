export interface UserDataInterface{
  coordenador: number | string;
  email: string;
  nome: string;
}


/*
coordenador: 2387
email: "prc@les.inf.puc-rio.br"
nome: "PAULO RC"
 */
