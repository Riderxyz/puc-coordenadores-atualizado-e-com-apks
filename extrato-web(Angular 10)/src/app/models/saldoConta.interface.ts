export interface SaldoContaInterface {
  conta: string;
  contaMae: string;
  descricao: string;
  descricaoBase: string;
}


export interface ContaDetailsInterface {
  codigo_projeto: number;
  nomeProjeto: string;
  conta: string;
  descricao: string;
  saldo: number;
}

export interface TotalContaInterface {
  conta: string;
  descricao: string;
  saldo: number;
}

