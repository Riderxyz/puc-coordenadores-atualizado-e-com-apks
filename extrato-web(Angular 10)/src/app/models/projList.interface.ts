export interface ProjListInterface {
  codigo: number;
  projeto: string;
  descricao?: string;
  coordenador: number;
  conta_principal: string;
  tipo_projeto: string;
}
