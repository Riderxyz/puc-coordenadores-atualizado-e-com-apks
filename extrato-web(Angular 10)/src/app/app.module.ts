import { FPLFInterceptor } from './services/http-interceptor.service';
import { ComponentsModule } from './components/components.module';
import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';

import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './services/login.service';
import { DataService } from './services/data.service';

import { AgGridModule } from 'ag-grid-angular';
import { ExtratosComponent } from './pages/extratos/extratos.component';
import { SaldoContasComponent } from './pages/saldo-contas/saldo-contas.component';
import { TotalContasComponent } from './pages/total-contas/total-contas.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
registerLocaleData(ptBr);
const form = [FormsModule, ReactiveFormsModule];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ExtratosComponent,
    SaldoContasComponent,
    TotalContasComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ...form,
    // Grid
    AgGridModule.withComponents([]),
    AppRoutingModule,
    MaterialModule,
    ComponentsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    DataService,
    LoginService,
    { provide: HTTP_INTERCEPTORS, useClass: FPLFInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
