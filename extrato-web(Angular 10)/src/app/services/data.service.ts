import { SaldoContaInterface } from './../models/saldoConta.interface';
import { ProjListInterface } from './../models/projList.interface';
import { UserDataInterface } from './../models/userData.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from './config';
import * as moment from 'moment';
@Injectable()
export class DataService {
  dataInicio = new Date();
  dataFim = new Date();
  LoggedUserData = {} as UserDataInterface;
  selectedProj: ProjListInterface = null;
  selectedConta: SaldoContaInterface = null;
  showHeader = false
;  constructor(private httpClient: HttpClient) {
    this.dataInicio = new Date(
      this.dataInicio.setMonth(this.dataInicio.getMonth() - 1)
    );
    this.userSavedData().then((res: UserDataInterface) => {
      console.log('Dentro do service', res);
      if (res) {
        this.LoggedUserData = res;
      }
    });
  }
  set saveUserData(storageData: UserDataInterface) {
    this.LoggedUserData = storageData;
    localStorage.setItem(
      config.localStorageKeys.userData,
      JSON.stringify(storageData)
    );
  }

  async userSavedData() {
    return JSON.parse(localStorage.getItem(config.localStorageKeys.userData));
  }

  async deleteUserData() {
    this.LoggedUserData = null;
    await localStorage.removeItem(config.localStorageKeys.userData);
  }

  get getUserProjList() {
    console.log(this.LoggedUserData);
    return this.httpClient.get(
      config.api.listaProjeto(this.LoggedUserData.coordenador)
    );
  }

  get getUserContaList() {
    return this.httpClient.get(
      config.api.listaConta(this.LoggedUserData.coordenador)
    );
  }
  get getExtrato() {
    return this.httpClient.get(
      config.api.extrato(
        this.selectedProj.codigo,
        moment(this.dataInicio).format('YYYY-MM-DD'),
        moment(this.dataFim).format('YYYY-MM-DD')
      )
    );
  }
  get getSaldoConta() {
    return this.httpClient.get(
      config.api.saldoConta(
        this.LoggedUserData.coordenador,
        this.selectedConta.conta,
        moment(this.dataFim).format('YYYY-MM-DD')
      )
    );
  }
  get getSaldoTotalConta() {
    console.log(this.LoggedUserData);
    return this.httpClient.get(
      config.api.saldoTotalConta(
        this.LoggedUserData.coordenador,
        moment(this.dataFim).format('YYYY-MM-DD')
      )
    );
  }

  get gerarExcelExtratos() {
    return this.httpClient.get(
      config.api.gerarExcelExtratos(
        this.selectedProj.codigo,
        moment(this.dataInicio).format('YYYY-MM-DD'),
        moment(this.dataFim).format('YYYY-MM-DD')
      )
    );
  }

  get gerarExcelSaldoConta() {
    console.log(this.selectedConta);

    return this.httpClient.get(
      config.api.gerarExcelSaldoConta(
        this.LoggedUserData.coordenador,
        this.selectedConta.conta,
        moment(this.dataFim).format('YYYY-MM-DD')
      )
    );
  }

  get gerarExcelTotalSaldoConta() {
    return this.httpClient.get(
      config.api.gerarExcelTotalSaldoConta(
        this.LoggedUserData.coordenador,
        moment(this.dataFim).format('YYYY-MM-DD')
      )
    );
  }
}
