import { UserDataInterface } from './../models/userData.interface';
import { LoginInterface } from './../models/login.interface';
import { environment } from './../../environments/environment';
import { ColDef } from 'ag-grid-community';

const url = 'http://139.82.24.10/MobServ/';
export const config = {
  localStorageKeys: {
    refreshToken: 'refreshToken',
    userData: 'userData',
  },
  ActivePages: {
    listaProjeto: false,
    extratoProjeto: false,
    saldoProjeto: false,
    totalContas: false,
  },
  api: {
    doLogin: (loginObj: LoginInterface) => {
      let doLoginUrl = url;
      doLoginUrl +=
        'api/usuarios?_usuario=' +
        loginObj.username +
        '&_senha=' +
        loginObj.password;
      return doLoginUrl;
    },
    editUserData: (userData: UserDataInterface) => {
      //http://139.82.24.10/MobServ/api/usuarios/GravarCoordenador/coordenador/2387/nomeusuario/PAULO RC/email/prc@les.inf.puc-rio.br
      //     http://139.82.24.10/MobServ/api/usuarios/GravarCoordenador/coordenador/2387/nomeusuario/PAULO%20RC/email/prc@les.inf.puc-rio.br/
      let editUserDataUrl = url;
      editUserDataUrl +=
        'api/usuarios/GravarCoordenador/coordenador/' +
        userData.coordenador +
        '/nomeusuario/' +
        userData.nome +
        '/email/' +
        userData.email +
        '/';
      console.log(editUserDataUrl);

      return editUserDataUrl;
    },
    listaProjeto: (coordenadorId: number | string) => {
      let listaProjetoUrl = url;

      const fullData = new Date();
      const parmdata =
        fullData.getFullYear() +
        '-' +
        fullData.getMonth() +
        '-' +
        fullData.getDate();
      listaProjetoUrl +=
        'api/projetos?coordenador=' + coordenadorId + '&data=' + parmdata;
      console.log('dentro de config', listaProjetoUrl);
      return listaProjetoUrl;
    },
    listaConta: (coordenadorId: number | string) => {
      const listaConta =
        url + 'api/Contas?coordenador=' + coordenadorId + '&listarmaes=true';
      //   http://139.82.24.10/MobServ/api/Contas?coordenador=2247&listarmaes=true
      return listaConta;
    },
    extrato: (selectedProj, dataInicio, dataFim) => {
      const extratoUrl =
        url +
        'api/extratos/getExtrato/projeto/' +
        selectedProj +
        '/di/' +
        dataInicio +
        '/df/' +
        dataFim +
        '/pagina/1/pagina_tamanho/9999';
      return extratoUrl;
    },
    saldoConta: (
      coordenadorId: number | string,
      contaID: number | string,
      dataFim: string
    ) => {
      let saldoConta = url;
      saldoConta +=
        'api/extratos/GetAnaliseContas/coordenador/' +
        coordenadorId +
        '/conta/' +
        contaID +
        '/data/' +
        dataFim;
      return saldoConta;

      //  http://139.82.24.10/MobServ/api/extratos/GetAnaliseContas/coordenador/2247/conta/32027/data/2020-09-21
    },
    saldoTotalConta: (coordenadorId: number | string, dataFim) => {
      const saldoContaUrl =
        url +
        'api/extratos/GetSaldosContas/coordenador/' +
        coordenadorId +
        '/data/' +
        dataFim;

      return saldoContaUrl;
    },
    gerarExcelSaldoConta: (
      coordenadorId: number | string,
      contaMae,
      dataFim
    ) => {
      let gerarExcelUrl = url;
      gerarExcelUrl +=
        'api/extratos/GetAnaliseContasExcel/coordenador/' +
        coordenadorId +
        '/conta/' +
        contaMae +
        '/data/' +
        dataFim;
      return gerarExcelUrl;
      //      http://139.82.24.10/MobServ/api/extratos/GetAnaliseContasExcel/coordenador/2247/conta/32006.000/data/2020-05-05
    },
    gerarExcelExtratos: (projetoId, dataInicio, dataFim) => {
      let gerarExcelUrl = url;
      gerarExcelUrl +=
        'api/extratos/getExtratoExcel/projeto/' +
        projetoId +
        `/di/${dataInicio}/df/${dataFim}/modo/2`;
      return gerarExcelUrl;
    },
    gerarExcelTotalSaldoConta: (coordenadorId: number | string, dataFim) => {
      //api/extratos/GetSaldosContasExcel/coordenador/2247/data/2020-09-21
      let gerarExcelUrl = url;
      gerarExcelUrl +=
        'api/extratos/GetSaldosContasExcel/coordenador/' +
        coordenadorId +
        '/data/' +
        dataFim;
      return gerarExcelUrl;
    },
    downloadExcel: (nome: string) => {
      let gerarExcelUrlUrl = url;
      gerarExcelUrlUrl += 'download/excel/' + nome;
      return gerarExcelUrlUrl;
    },
  },
  colunas: {
    extratoCol: [
      {
        headerName: 'id'.toLocaleUpperCase(),
        field: 'id',
        width: 30,
        autoHeight: true,
        hide: true,
      },
      {
        headerName: 'Data'.toLocaleUpperCase(),
        field: 'data',
        width: 30,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'Fatura'.toLocaleUpperCase(),
        field: 'fatura',
        width: 30,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'histórico'.toLocaleUpperCase(),
        field: 'texto',
        width: 100,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'receita'.toLocaleUpperCase(),
        field: 'receita',
        width: 30,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'despesa'.toLocaleUpperCase(),
        field: 'despesa',
        width: 30,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'saldo'.toLocaleUpperCase(),
        field: 'saldo',
        width: 30,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
    ] as ColDef[],
    saldoContaCol: [
      {
        headerName: 'id'.toLocaleUpperCase(),
        field: 'codigo_projeto',
        width: 30,
        autoHeight: true,
        hide: true,
      },
      {
        headerName: 'nome'.toLocaleUpperCase(),
        field: 'descricao',
        width: 300,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'saldo'.toLocaleUpperCase(),
        field: 'saldo',
        width: 120,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
    ] as ColDef[],
    totalConta: [
      {
        headerName: 'id'.toLocaleUpperCase(),
        field: 'conta',
        width: 30,
        autoHeight: true,
        hide: true,
      },
      {
        headerName: 'nome'.toLocaleUpperCase(),
        field: 'descricao',
        width: 300,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
      {
        headerName: 'saldo'.toLocaleUpperCase(),
        field: 'saldo',
        width: 120,
        autoHeight: true,
        cellStyle: { 'font-size': '1.2em' },
      },
    ] as ColDef[],
  },
};
