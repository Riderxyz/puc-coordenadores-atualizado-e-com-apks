import { ToastService } from './toast.service';
import { UserDataInterface } from './../models/userData.interface';
import { LoginInterface } from './../models/login.interface';
import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { config } from './config';

@Injectable()
export class LoginService {
  constructor(
    private httpClient: HttpClient,
    private dataSrv: DataService,
    private router: Router,
    private toastSrv: ToastService
  ) {
    setTimeout(async () => {
      const currentUser: UserDataInterface = await this.dataSrv.userSavedData();
      if (currentUser) {
        console.log('opa, Eu existo');
        this.dataSrv.showHeader = true;
        this.toastSrv.showToastSucess('Usuario recuperado com sucesso');
        this.router.navigateByUrl('/home');
      }
    }, 500);
  }
  doLogin(userData: LoginInterface) {
    this.httpClient.get(config.api.doLogin(userData)).subscribe(
      (resLogin: any) => {
        console.log(JSON.parse(resLogin).tabUsuario[0]);
        this.dataSrv.saveUserData = resLogin.tabUsuario[0];
        this.router.navigateByUrl('/home');
        this.toastSrv.showToastSucess('Usuario Logado com sucesso');
      },
      (err) => {
        this.toastSrv.showToastError(
          'Não foi possivel efetuar o seu login',
          'Tente novamente mais tarde'
        );
        console.log(err);
      }
    );
  }
  editUserData(userEditObj: UserDataInterface) {
    this.httpClient.get(config.api.editUserData(userEditObj)).subscribe(
      (resUser: any) => {
        console.log(
          'Teste de valores0asasa',
          resUser.tabUsuario[0]
        );
        this.toastSrv.showToastSucess('Dados de Usuario salvos com sucesso');
        this.dataSrv.saveUserData = resUser.tabUsuario[0];
      },
      (err) => {
        this.toastSrv.showToastError(
          'Não foi possivel salvar os seus dados',
          'Por favor, tente novamente mais tarde'
        );
      }
    );
  }
  logOut() {
    this.dataSrv.deleteUserData();
    this.router.navigateByUrl('/login');
  }
}
