import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
@Injectable({ providedIn: 'root' })
export class ToastService {
  constructor() {}

  showToastSucess(title, subtitle?) {
    if (subtitle === undefined) {
      subtitle = null;
    }
    Swal.fire({
      title: title + '!',
      text: subtitle,
      icon: 'success',
      customClass: {
        title: 'onSucessToast',
        content: 'onSucessToast',
      },
      timerProgressBar: true,
      timer: 3000,
      showConfirmButton: false,
      showCancelButton: false,
      background: 'linear-gradient(315deg, #7ee8fa 0%, #80ff72 74%)',
      toast: true,

      //  timer: 3000,
      position: 'top-right',
    });
  }

  showToastError(title, subtitle?) {
    if (subtitle === undefined) {
      subtitle = null;
    }
    Swal.fire({
      title: title + '.',
      text: subtitle,
      icon: 'error',
      timer: 3000,
      timerProgressBar: true,
      customClass: {
        title: 'onErrorToast',
        content: 'onErrorToast',
      },
      background: 'linear-gradient(to left, #ff0095, #ff0000)',
      /*             showCancelButton: true, */
      toast: true,
      //  timer: 3000,
      position: 'top-right',
    });
  }
}
