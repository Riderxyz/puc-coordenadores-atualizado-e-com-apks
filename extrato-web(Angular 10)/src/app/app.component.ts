import { DataService } from './services/data.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'extrato-web';
  constructor(public dataSrv: DataService) {
    moment.locale('pt-br');

  }
  ngOnInit() {
    this.dataSrv
  }
  ngOnDestroy() {

  }
}
