import { DataService } from './../../services/data.service';
import { LoginService } from './../../services/login.service';
import { LoginInterface } from './../../models/login.interface';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],

})
export class LoginComponent implements OnInit, OnDestroy {
  LoginObj: LoginInterface = {} as LoginInterface;
  constructor(private loginSrv: LoginService, public dataSrv: DataService) {}

  ngOnInit(): void {
    this.dataSrv.showHeader = false;
  }
  onLogin() {
    console.log(123456789);
    this.loginSrv.doLogin(this.LoginObj);
  }

  ngOnDestroy() {
    this.dataSrv.showHeader = true;
  }
}
