import { ToastService } from './../../services/toast.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginService } from './../../services/login.service';
import { DataService } from './../../services/data.service';
import { ListaProjetoComponent } from './../../components/lista-projeto/lista-projeto.component';
import { EditarUsuarioComponent } from './../../components/editar-usuario/editar-usuario.component';
import * as moment from 'moment';
import { flipOnEnterAnimation, bounceInUpOnEnterAnimation} from 'angular-animations';
@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [flipOnEnterAnimation(), bounceInUpOnEnterAnimation()]
})
export class HomeComponent implements OnInit {
  arrButtons: {
    nome: string;
    icone: string;
    cor: string;
    key: number;
  }[] = [
    {
      nome: 'Extrato dos Projetos',
      icone: './assets/extratoProjeto.png',
      cor: '#7986cb',
      key: 1,
    },
    {
      nome: 'Saldo dos Projetos',
      icone: './assets/saldoConta.png',
      cor: '#283593',
      key: 2,
    },
    {
      nome: 'Saldo Total das Contas',
      icone: './assets/totalConta.png',
      cor: '#283593',
      key: 3,
    },
    {
      nome: 'Dados do Coordenador',
      icone: './assets/usuario.png',
      cor: '#8c9eff',
      key: 4,
    },
  ];
  /*   dataInicio = new Date(2018);
  dataFim = new Date(); */
  constructor(
    public dialog: MatDialog,
    public dataSrv: DataService,
    private loginSrv: LoginService,
    private router: Router,
    private toastSrv: ToastService
  ) {}
  ngOnInit(): void {
    /*     this.loginSrv.doLogin({
      //username: '2247',
      //password: '5515',
      // username: '2387',
     // password: '123456',
    }); */
   /*  this.dataSrv.showHeader = true; */
  }
  switchEvent(key: number) {
    const dataInicio = this.dataSrv.dataInicio.getTime();
    const dataFim = this.dataSrv.dataFim.getTime();
    if (dataInicio > dataFim) {
      this.toastSrv.showToastError(
        'A data Inicio não pode ser maior que a data final'
      );
    } else {
      switch (key) {
        case 1:
          this.openListProj();
          break;
        case 2:
          this.router.navigateByUrl('saldoConta');
          break;
        case 3:
          this.router.navigateByUrl('totalConta');
          break;
        case 4:
          this.openEditUser();
          break;
      }
    }
  }
  openListProj() {
    const dialogRef = this.dialog.open(ListaProjetoComponent);
    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openEditUser() {
    const dialogRef = this.dialog.open(EditarUsuarioComponent);
    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
  }

  sair() {
    this.dataSrv.deleteUserData().then(() => {
      this.router.navigateByUrl('/login');
    });
  }
}
