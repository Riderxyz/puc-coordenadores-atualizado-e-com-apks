import { ToastService } from './../../services/toast.service';
import { ContaDetailsInterface } from './../../models/saldoConta.interface';
import { SaldoContaInterface } from './../../models/saldoConta.interface';
import { ColDef } from 'ag-grid-community';
import { DataService } from './../../services/data.service';
import { ExtratoInterface } from './../../models/extrato.interface';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { config } from 'src/app/services/config';
import { Router } from '@angular/router';
import { ExcelDownloadComponent } from './../../components/excel-download/excel-download.component';
import {
  MatBottomSheet,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { zoomInDownOnEnterAnimation } from 'angular-animations';
@Component({
  templateUrl: './saldo-contas.component.html',
  styleUrls: ['./saldo-contas.component.scss'],
  animations: [zoomInDownOnEnterAnimation()]
})
export class SaldoContasComponent implements OnInit {
  saldoContaList: SaldoContaInterface[] = [];
  contaDetailsList: ContaDetailsInterface[] = [];
  contaSelecionada: SaldoContaInterface = null as SaldoContaInterface;
  constructor(
    private dataSrv: DataService,
    private router: Router,
    private bottomSheet: MatBottomSheet,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.getUserContaList.subscribe(
      (saldoConta: any) => {
        const saldoContaArr = saldoConta.tabContas;
        console.log(JSON.stringify(saldoContaArr));
        this.saldoContaList = saldoContaArr;
      },
      (err) => {
        this.toastSrv.showToastError(
          'Não foi possivel carregar a lista de contas.',
          'Tente novamente mais tarde'
        );
      }
    );
  }

  get formatTitle() {
    let title = 'Saldo de Projetos da conta ';
    if (this.contaSelecionada !== null) {
      title += this.contaSelecionada.descricao;
      return title;
    } else {
      title = 'Saldo de Projetos';
      return title;
    }
  }

  showContaDetails() {
    console.log(this.contaSelecionada);
    this.dataSrv.selectedConta = this.contaSelecionada;
    this.dataSrv.getSaldoConta.subscribe(
      (saldoConta: any) => {
        this.contaDetailsList = saldoConta.tabAnaliseContas;
        console.log(this.contaDetailsList);
        this.toastSrv.showToastSucess(
          'Detalhes da Conta carregados com sucesso'
        );
      },
      (err) => {
        this.toastSrv.showToastError(
          'Não foi possivel carregar os detalhes da Conta',
          'Tente novamente mais tarde'
        );
      }
    );
  }

  get createColumnDefs(): ColDef[] {
    return config.colunas.saldoContaCol;
  }
  voltar() {
    this.router.navigateByUrl('/home');
  }
  gerarExcel() {
    this.bottomSheet.open(ExcelDownloadComponent, {
      data: {
        comeFrom: 'saldoConta',
        data: null,
      },
    });
  }
}
