import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaldoContasComponent } from './saldo-contas.component';

describe('SaldoContasComponent', () => {
  let component: SaldoContasComponent;
  let fixture: ComponentFixture<SaldoContasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaldoContasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaldoContasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
