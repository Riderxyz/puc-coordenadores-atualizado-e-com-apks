import { ToastService } from './../../services/toast.service';
import { ColDef } from 'ag-grid-community';
import { DataService } from './../../services/data.service';
import { ExtratoInterface } from './../../models/extrato.interface';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { config } from 'src/app/services/config';
import { Router } from '@angular/router';
import { ExcelDownloadComponent } from './../../components/excel-download/excel-download.component';
import {
  MatBottomSheet,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { zoomInDownOnEnterAnimation } from 'angular-animations';

@Component({
  templateUrl: './extratos.component.html',
  styleUrls: ['./extratos.component.scss'],
  animations: [zoomInDownOnEnterAnimation()]
})
export class ExtratosComponent implements OnInit {
  extratoList: ExtratoInterface[] = [];
  saldo = {} as ExtratoInterface;
  constructor(
    private dataSrv: DataService,
    private router: Router,
    private bottomSheet: MatBottomSheet,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.getExtrato.subscribe(
      (extrato: any) => {
        this.extratoList = extrato.tabExtrato;
        this.extratoList.forEach((element) => {
          element.data = moment(element.data).format('L');
        });
        this.saldo = this.extratoList[0];

        this.toastSrv.showToastSucess('Extratos carregados');
      },
      (err) => {
        this.toastSrv.showToastError(
          'Não foi possivel carregar os Extratos do projeto',
          'Tente novamente mais tarde'
        );
      }
    );
  }

  get formatTitle() {
    let title = '';
    title += 'Projeto ' + this.dataSrv.selectedProj.projeto;
    title +=
      ' - ' +
      moment(this.dataSrv.dataInicio).format('L') +
      ' a ' +
      moment(this.dataSrv.dataFim).format('L');
    return title;
  }

  get createColumnDefs(): Array<ColDef> {
    return config.colunas.extratoCol;
  }

  voltar() {
    this.router.navigateByUrl('/home');
  }
  gerarExcel() {
    this.bottomSheet.open(ExcelDownloadComponent, {
      data: {
        comeFrom: 'extrato',
        data: null,
      },
    });
  }
}
