import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalContasComponent } from './total-contas.component';

describe('TotalContasComponent', () => {
  let component: TotalContasComponent;
  let fixture: ComponentFixture<TotalContasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalContasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalContasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
