import { ToastService } from './../../services/toast.service';
import { TotalContaInterface } from './../../models/saldoConta.interface';
import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { DataService } from './../../services/data.service';
import * as moment from 'moment';
import { config } from 'src/app/services/config';
import { Router } from '@angular/router';
import { ExcelDownloadComponent } from './../../components/excel-download/excel-download.component';
import {
  MatBottomSheet,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { zoomInDownOnEnterAnimation } from 'angular-animations';
@Component({
  templateUrl: './total-contas.component.html',
  styleUrls: ['./total-contas.component.scss'],
  animations:[zoomInDownOnEnterAnimation()]
})
export class TotalContasComponent implements OnInit {
  totalContaList: TotalContaInterface[] = [];
  saldo = 0;
  constructor(
    private dataSrv: DataService,
    private router: Router,
    private bottomSheet: MatBottomSheet,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.getSaldoTotalConta.subscribe(
      (totalConta: any) => {
        console.log(JSON.parse(totalConta).tabSaldoContas);
        this.totalContaList = totalConta.tabSaldoContas;
        this.saldo = 0;
        this.totalContaList.forEach((element) => {
          this.saldo += element.saldo;
        });
        this.toastSrv.showToastSucess('Dados de contas carregado com sucesso');
      },
      (err) => {
        this.toastSrv.showToastError(
          'Não foi possivel recuperar os dados das suas contas',
          'Tente novamente mais tarde'
        );
      }
    );
  }
  get formatTitle() {
    let title = 'Total das Contas. Saldo Atual é de ';

    return title;
  }

  get createColumnDefs() {
    return config.colunas.totalConta;
  }

  voltar() {
    this.router.navigateByUrl('/home');
  }
  gerarExcel() {
    this.bottomSheet.open(ExcelDownloadComponent, {
      data: {
        comeFrom: 'totalConta',
        data: null,
      },
    });
  }
}
