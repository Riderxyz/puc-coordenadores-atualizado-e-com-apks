import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { LoginInterface } from "../models/login.interface";
import { DataService } from "./data.service";
import { NavController, AlertController } from "@ionic/angular";
environment;
@Injectable({ providedIn: "root" })
export class LoginService {
  showLoad = true;
  constructor(
    private httpClient: HttpClient,
    private dataSrv: DataService,
    private navCtrl: NavController,
    private alertController: AlertController
  ) {

    setTimeout(async () => {
      const currentUser = await this.dataSrv.userSavedData();
      console.log("assasas", currentUser);
      if (currentUser) {
        console.log("opa, Eu existo");
      setTimeout(() => {
        this.showLoad = false;
      }, 1500);
      setTimeout(() => {
        this.navCtrl.navigateRoot("/home");
      }, 1600);
      } else {
        this.showLoad = false;
      }
    }, 500);
  }
  async doLogin(userData: LoginInterface) {
    this.httpClient
      .get(
        environment.baseUrl +
          "/api/usuarios?_usuario=" +
          userData.username +
          "&_senha=" +
          userData.password
      )
      .subscribe(
        (resLogin: any) => {
        console.log(JSON.parse(resLogin).tabUsuario[0]);
        this.dataSrv.saveUserData = JSON.parse(resLogin).tabUsuario[0];
        this.navCtrl.navigateRoot("/home");
      }, (async (err)=> {
        console.log(err)
/*         async presentAlert() { */
          const alert = await this.alertController.create({
            header: 'Erro de login',
            message: 'Tente novamente mais tarde',
            buttons: ['OK']
          });
          await alert.present();
/*         } */
      }));
  }
  logOut() {
    this.dataSrv.deleteUserData();
    this.navCtrl.navigateRoot('/login');
  }
}
