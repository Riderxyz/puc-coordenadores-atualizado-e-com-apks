import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { UserDataInterface } from "../models/userData.interface";
import { environment } from "src/environments/environment";
import * as moment from 'moment';
@Injectable({ providedIn: "root" })
export class DataService {
  dataInicio = null;
  dataFim = null;
  LoggedUserData = {} as UserDataInterface;
  selectedProj = null;
  constructor(private httpClient: HttpClient, private storage: Storage) {
    this.userSavedData().then((res: UserDataInterface) => {
      console.log("Dentro do service", res);
      if (res) {
        this.LoggedUserData = res;
      }
    });
  }
  set saveUserData(storageData: UserDataInterface) {
    this.LoggedUserData = storageData;
    this.storage.set(environment.storageKeys.userData, storageData);
  }

  async userSavedData() {
    return this.storage.get(environment.storageKeys.userData);
  }

  async deleteUserData() {
    this.LoggedUserData = null;
    await this.storage.remove(environment.storageKeys.userData);
  }

  get getUserProjList() {
    console.log(this.LoggedUserData);
    return this.httpClient.get(
      environment.baseUrl +
        "/api/projetos?coordenador=" +
        this.LoggedUserData.coordenador +
        "&data=" +
        moment().format('YYYY-MM-DD')
    );
  }
  get getExtrato() {
    /*     const fakeurl = 'http://139.82.24.10/MobServ/api/extratos/getExtrato/projeto/13901/di/2019-11-15/df/2020-11-15/pagina/1/pagina_tamanho/9999'

  return this.httpClient.get(fakeurl); */
  console.log(this.dataInicio);
  console.log(this.dataFim);
  
    return this.httpClient.get(
      environment.baseUrl +
        "api/extratos/getExtrato/projeto/" +
        this.selectedProj +
        "/di/" +
        this.dataInicio +
        "/df/" +
        this.dataFim +
        "/pagina/1/pagina_tamanho/9999"
    );
  }
  get getSaldoConta() {
/*     const fakeURL = 'http://139.82.24.10/MobServ/api/extratos/GetSaldosContas/coordenador/2247/data/2020-09-09';
    console.log(fakeURL)
    return this.httpClient.get(fakeURL); */
 return this.httpClient.get(
      environment.baseUrl +
        "api/extratos/GetSaldosContas/coordenador/" +
        this.LoggedUserData.coordenador +
        "/data/" +
        this.dataFim
    );
  }
}
