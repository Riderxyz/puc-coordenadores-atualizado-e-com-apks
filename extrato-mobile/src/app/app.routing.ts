import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'list-projetos',
    loadChildren: () => import('./pages/list-projetos/list-projetos.module').then( m => m.ListProjetosPageModule)
  },
  {
    path: 'extratos',
    loadChildren: () => import('./pages/extratos/extratos.module').then( m => m.ExtratosPageModule)
  },
  {
    path: 'saldo-conta',
    loadChildren: () => import('./pages/saldo-conta/saldo-conta.module').then( m => m.SaldoContaPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
