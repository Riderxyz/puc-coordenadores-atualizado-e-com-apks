import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaldoContaPageRoutingModule } from './saldo-conta-routing.module';

import { SaldoContaPage } from './saldo-conta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SaldoContaPageRoutingModule
  ],
  declarations: [SaldoContaPage]
})
export class SaldoContaPageModule {}
