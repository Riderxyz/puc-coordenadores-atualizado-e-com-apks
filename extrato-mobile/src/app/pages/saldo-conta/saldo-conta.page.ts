import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { SaldoContaInterface } from 'src/app/models/saldoContas.interface';

@Component({
  selector: "app-saldo-conta",
  templateUrl: "./saldo-conta.page.html",
  styleUrls: ["./saldo-conta.page.scss"],
})
export class SaldoContaPage implements OnInit {
  saldoContaList:SaldoContaInterface[] = [];
  saldoHeader = {
    conta: 0,
    descricao:'',
    saldo:0
  } as SaldoContaInterface;
  constructor(public dataSrv: DataService)
  {}

  ngOnInit() {
    this.dataSrv.getSaldoConta.subscribe((resSaldo: string) => {
      console.log(JSON.parse(resSaldo));
      this.saldoContaList = JSON.parse(resSaldo).tabSaldoContas;

      this.saldoContaList.forEach((element) => {
        console.log(element)
        this.saldoHeader.saldo += element.saldo;
        console.log(this.saldoHeader);
      });
    });
  }
}
