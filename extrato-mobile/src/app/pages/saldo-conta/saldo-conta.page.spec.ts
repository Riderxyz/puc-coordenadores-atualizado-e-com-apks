import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaldoContaPage } from './saldo-conta.page';

describe('SaldoContaPage', () => {
  let component: SaldoContaPage;
  let fixture: ComponentFixture<SaldoContaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaldoContaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaldoContaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
