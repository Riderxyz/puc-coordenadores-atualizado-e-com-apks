import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaldoContaPage } from './saldo-conta.page';

const routes: Routes = [
  {
    path: '',
    component: SaldoContaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaldoContaPageRoutingModule {}
