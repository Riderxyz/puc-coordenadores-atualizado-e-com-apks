import { Component, OnInit } from "@angular/core";
import { LoginService } from "src/app/services/login.service";
import { DataService } from "src/app/services/data.service";
import { NavController, ToastController } from "@ionic/angular";
import { fadeInUpOnEnterAnimation } from "angular-animations";
import { environment } from "src/environments/environment";
import * as moment from 'moment';
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
  animations: [fadeInUpOnEnterAnimation()],
})
export class HomePage implements OnInit {
  title = null as string;
  constructor(
    private loginSrv: LoginService,
    private dataSrv: DataService,
    public navCtrl: NavController,
    private toastController: ToastController
  ) {}
  ngOnInit() {
  //  this.showTitle();

 /*     */

    setTimeout(() => {
      console.log(environment);
      
      this.loginSrv.doLogin({
        username: environment.userTest.username,
        password: environment.userTest.password,
      });
    }, 1500);
  }

  setDate(ev, path) {
    console.log(ev.value);
    const dataTransformada = moment(ev.value).format('YYYY-MM-DD');
    console.log('sd',moment(ev.value).format('YYYY-MM-DD'));
    switch (path) {
      case 0:
        this.dataSrv.dataInicio = dataTransformada;
        break;
      case 1:
        this.dataSrv.dataFim = dataTransformada;
        break;
    }
  }
/* 
  async showTitle() {
    const resTitle = await this.dataSrv.userSavedData();
    console.log("qw", resTitle);
    this.title = resTitle.nome;
  }
 */
  async goToListProj() {
    if (this.dataSrv.dataInicio !== null && this.dataSrv.dataFim !== null) {
      this.navCtrl.navigateForward("/list-projetos");
    } else {
      const toast = await this.toastController.create({
        message: "Defina uma data de inicio e fim para avançar",
        duration: 2000,
        position: "middle",
      });
      toast.present();
    }
  }

  async gotToSaldoContas() {
    if (this.dataSrv.dataInicio !== null && this.dataSrv.dataFim !== null) {
      this.navCtrl.navigateForward("/saldo-conta");
    } else {
      const toast = await this.toastController.create({
        message: "Defina uma data de inicio e fim para avançar",
        duration: 2000,
        position: "middle",
      });
      toast.present();
    }
  }

  logOut() {
    this.loginSrv.logOut();
    /*     this.loginSrv.doLogin({
      username: "2247",
      password: "5515",
    }); */
  }
}
