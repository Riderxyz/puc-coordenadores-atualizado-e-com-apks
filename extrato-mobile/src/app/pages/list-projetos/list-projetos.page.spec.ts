import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListProjetosPage } from './list-projetos.page';

describe('ListProjetosPage', () => {
  let component: ListProjetosPage;
  let fixture: ComponentFixture<ListProjetosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProjetosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListProjetosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
