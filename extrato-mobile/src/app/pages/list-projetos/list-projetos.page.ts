import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { ProjListInterface } from "src/app/models/projList.interface";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-list-projetos",
  templateUrl: "./list-projetos.page.html",
  styleUrls: ["./list-projetos.page.scss"],
})
export class ListProjetosPage implements OnInit {
  projList: ProjListInterface[] = [];
  constructor(public dataSrv: DataService, private navCtrl: NavController) {}

  ngOnInit() {
    this.dataSrv.getUserProjList.subscribe((proj: any) => {
      this.projList = JSON.parse(proj).tabProjetos;
    });
  }

  selectedProj(event: ProjListInterface) {
    console.log(event);
    this.dataSrv.selectedProj = event.codigo;
    this.navCtrl.navigateForward("/extratos");
  }
}
