import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListProjetosPageRoutingModule } from './list-projetos-routing.module';

import { ListProjetosPage } from './list-projetos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListProjetosPageRoutingModule
  ],
  declarations: [ListProjetosPage]
})
export class ListProjetosPageModule {}
