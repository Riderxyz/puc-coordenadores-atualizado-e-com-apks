import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListProjetosPage } from './list-projetos.page';

const routes: Routes = [
  {
    path: '',
    component: ListProjetosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListProjetosPageRoutingModule {}
