import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExtratosPage } from './extratos.page';

describe('ExtratosPage', () => {
  let component: ExtratosPage;
  let fixture: ComponentFixture<ExtratosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtratosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExtratosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
