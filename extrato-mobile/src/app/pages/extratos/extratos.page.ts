import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ExtratoInterface } from 'src/app/models/extrato.interface';
import * as moment from 'moment';
@Component({
  selector: 'app-extratos',
  templateUrl: './extratos.page.html',
  styleUrls: ['./extratos.page.scss'],
})
export class ExtratosPage implements OnInit, AfterViewInit {
extratoList:ExtratoInterface[] = [];
saldo = {} as ExtratoInterface;
  constructor(private dataSrv: DataService) { }

  ngOnInit() {

    this.dataSrv.getExtrato.subscribe((extrato:any) => {
      console.log(extrato)
      this.extratoList = JSON.parse(extrato).tabExtrato;
      this.saldo = this.extratoList[0];
      console.log(this.extratoList);

    })
  }
  ngAfterViewInit() {
  }

}
