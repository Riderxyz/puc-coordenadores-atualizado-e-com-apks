import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExtratosPageRoutingModule } from './extratos-routing.module';

import { ExtratosPage } from './extratos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExtratosPageRoutingModule
  ],
  declarations: [ExtratosPage]
})
export class ExtratosPageModule {}
