import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExtratosPage } from './extratos.page';

const routes: Routes = [
  {
    path: '',
    component: ExtratosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExtratosPageRoutingModule {}
