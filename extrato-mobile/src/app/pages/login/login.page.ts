import { Component, OnInit } from "@angular/core";
import { LoginInterface } from "src/app/models/login.interface";
import { NgForm } from "@angular/forms";
import {
  fadeOutUpOnLeaveAnimation,
  rotateOutOnLeaveAnimation,
} from "angular-animations";
import { LoginService } from "src/app/services/login.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
  animations: [fadeOutUpOnLeaveAnimation(), rotateOutOnLeaveAnimation()],
})
export class LoginPage implements OnInit {
  loginObj: LoginInterface = {
    username: "",
    password: "",
  };

  constructor(public loginSrv: LoginService) {}
  ngOnInit() {}

  onSubmit(form: NgForm) {
    console.log(this.loginObj);

    this.loginSrv.doLogin(this.loginObj);
  }
}
