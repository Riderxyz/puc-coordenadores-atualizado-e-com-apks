export interface ExtratoInterface {
  linha: number;
  id: number;
  projeto: string;
  nome: string;
  fatura?: string;
  data: string;
  texto: string;
  receita: number;
  despesa: number;
  saldo: number;
  valor: number;
}
