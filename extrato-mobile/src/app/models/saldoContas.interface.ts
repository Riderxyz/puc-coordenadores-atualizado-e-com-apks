export interface SaldoContaInterface {
  conta: string | number;
  descricao: string;
  saldo: number;
}
