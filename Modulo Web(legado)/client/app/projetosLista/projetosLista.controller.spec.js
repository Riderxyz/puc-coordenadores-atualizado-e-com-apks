'use strict';

describe('Controller: ProjetosListaCtrl', function () {

  // load the controller's module
  beforeEach(module('extratoWebApp'));

  var ProjetosListaCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProjetosListaCtrl = $controller('ProjetosListaCtrl', {
      $scope: scope;
    })
  }))

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
