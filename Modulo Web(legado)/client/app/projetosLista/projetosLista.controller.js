'use strict';

angular.module('extratoWebApp')
	.controller('ProjetosListaCtrl', ['$scope', '$mdDialog','$rootScope', 'utilSrv', '$http', '$log', function($scope, $mdDialog, $rootScope, utilSrv, $http, $log) {
		$scope.dtInicio = new Date();
		$scope.dtFim = new Date();
		$scope.projetos = [];

		$scope.exibirExtrato = function(modo) {
			$rootScope.$broadcast('extratoProjeto',modo); 
		}

		$http.get(utilSrv.getUrlPadrao() + '/api/projetos?coordenador=' + utilSrv.coordenador()).then(
			function(data) {
				$scope.projetos = JSON.parse(data.data).tabProjetos;
			}
		);

		$scope.showAlert = function(ev) {
			$mdDialog.show(
				$mdDialog.alert()
				.title('This is an alert title')
				.content('You can specify some description text in here.')
				.ariaLabel('Password notification')
				.ok('Got it!')
				.targetEvent(ev)
			);
		};
	}]);