'use strict';

angular.module('extratoWebApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('contas', {
        url: '/contas',
        templateUrl: 'app/contas/contas.html',
        controller: 'ContasCtrl'
      });
  });