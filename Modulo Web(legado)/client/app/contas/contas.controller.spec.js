'use strict';

describe('Controller: ContasCtrl', function () {

  // load the controller's module
  beforeEach(module('extratoWebApp'));

  var ContasCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ContasCtrl = $controller('ContasCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
