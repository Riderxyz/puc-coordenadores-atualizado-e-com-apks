'use strict';

angular.module('extratoWebApp')
  .controller('ContasCtrl',['$scope', '$rootScope','utilSrv', '$http','$log', function ($scope, $rootScope, utilSrv, $http, $log) {
		$scope.url = utilSrv.getUrlPadrao();
		$scope.contas = [];
		$http.get(utilSrv.getUrlPadrao()+ '/api/contas?coordenador=' + utilSrv.coordenador()).then(
			function(data) {
				$scope.contas = JSON.parse(data.data).tabContas;
			}
		);

		$scope.exibirExtrato = function(modo) {
			$rootScope.$broadcast('extratoConta', modo);
		}
  }]);
