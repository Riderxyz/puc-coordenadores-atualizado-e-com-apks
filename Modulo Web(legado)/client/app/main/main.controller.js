'use strict';

angular.module('extratoWebApp')
  .controller('MainCtrl', ['$scope',
    '$http', '$log', 'utilSrv',
    function($scope, $http, $log, utilSrv) {
      $scope.cor = 0;
      $scope.exibindoProjeto = false;
      $scope.exibindoConta = false;
      $scope.exibindoRubrica = false;

      $scope.$on('extratoProjeto', function() {
        $scope.exibindoProjeto = true;
        $scope.exibindoConta = false;
        $scope.exibindoRubrica = false;
      });

      $scope.$on('extratoConta', function() {
        $scope.exibindoProjeto = false;
        $scope.exibindoConta = true;
        $scope.exibindoRubrica = false;
      });      

      $scope.exibirProjeto = function() {
        return utilSrv.ExibindoProjeto;
      };

      $scope.getUrl = function() {
        if ($scope.exibindoProjeto) {
          return 'app/extratoProjeto/extratoProjeto.html';
        } else {
          if ($scope.exibindoConta) {
            return 'app/extratoConta/extratoConta.html';
          } else {
            return '';
          }
        }
      };

      $scope.getColor = function(cor) {
        return utilSrv.getColor(cor);
      };

      $scope.ChangeSize = function(modo) {
        if (modo === 1) {
          $scope.TamanhoProjetos = 1;
          $scope.TamanhoRubricas = 3;
        } else {
          $scope.TamanhoProjetos = 3;
          $scope.TamanhoRubricas = 1;
        }
      };

    }
  ]);