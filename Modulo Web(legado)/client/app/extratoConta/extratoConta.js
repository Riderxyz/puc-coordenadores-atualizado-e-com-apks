'use strict';

angular.module('extratoWebApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('extratoConta', {
        url: '/extratoConta',
        templateUrl: 'app/extratoConta/extratoConta.html',
        controller: 'ExtratoContaCtrl'
      });
  });