'use strict';

describe('Controller: ExtratoContaCtrl', function () {

  // load the controller's module
  beforeEach(module('extratoWebApp'));

  var ExtratoContaCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExtratoContaCtrl = $controller('ExtratoContaCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
