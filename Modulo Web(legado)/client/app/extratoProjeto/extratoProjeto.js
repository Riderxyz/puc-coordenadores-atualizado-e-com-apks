'use strict';

angular.module('extratoWebApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('extratoProjeto', {
        url: '/extratoProjeto',
        templateUrl: 'app/extratoProjeto/extratoProjeto.html',
        controller: 'ExtratoProjetoCtrl'
      });
  });