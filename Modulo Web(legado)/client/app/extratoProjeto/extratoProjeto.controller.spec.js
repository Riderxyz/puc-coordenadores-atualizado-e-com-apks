'use strict';

describe('Controller: ExtratoProjetoCtrl', function () {

  // load the controller's module
  beforeEach(module('extratoWebApp'));

  var ExtratoProjetoCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExtratoProjetoCtrl = $controller('ExtratoProjetoCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
