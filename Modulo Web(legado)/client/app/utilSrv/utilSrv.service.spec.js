'use strict';

describe('Service: utilSrv', function () {

  // load the service's module
  beforeEach(module('extratoWebApp'));

  // instantiate service
  var utilSrv;
  beforeEach(inject(function (_utilSrv_) {
    utilSrv = _utilSrv_;
  }));

  it('should do something', function () {
    expect(!!utilSrv).toBe(true);
  });

});
