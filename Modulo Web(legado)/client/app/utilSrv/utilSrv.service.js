'use strict';

angular.module('extratoWebApp')
/*  .constant('projeto', 0);
  .constant('conta', 1);
  .constant('rubrica', 2);*/

.service('utilSrv', function() {

  var servico = this;
  var prod = 'nao';
  servico.exibindoProjeto = false;
  servico.exibindoConta = false;
  servico.exibindoRubrica = false;

/*  servico.exibirExtrato = function(extrato) {
    console.log('exibir Extrato')
    switch (extrato) {
      case 0:
        servico.exibindoProjeto = true;
        servico.exibindoConta = false;
        servico.exibindoRubrica = false;
        $rootScope.$broadcast('extratoProjeto');
        break;
      case 1:
        servico.exibindoProjeto = false;
        servico.exibindoConta = true;
        servico.exibindoRubrica = false;
        break;
      case 2:
        servico.exibindoProjeto = false;
        servico.exibindoConta = false;
        servico.exibindoRubrica = true;
        break;
      default:

    }
  };*/


  servico.coordenador = function() {
    return 2387;
  };

  servico.getColor = function(cor) {
    var _d = (cor + 1) % 11;
    var bg = '';

    switch (_d) {
      case 1:
        bg = 'red';
        break;
      case 2:
        bg = 'green';
        break;
      case 3:
        bg = 'darkBlue';
        break;
      case 4:
        bg = 'blue';
        break;
      case 5:
        bg = 'yellow';
        break;
      case 6:
        bg = 'pink';
        break;
      case 7:
        bg = 'darkBlue';
        break;
      case 8:
        bg = 'purple';
        break;
      case 9:
        bg = 'deepBlue';
        break;
      case 10:
        bg = 'lightPurple';
        break;
      default:
        bg = 'yellow';
        break;
    }

    return bg;
  };

  servico.getUrlPadrao = function() {
    if (prod === 'sim') {
      return 'http://139.82.24.10/MobServ';
    } else {
      return 'http://localhost:19017';
    }
  };

  // AngularJS will instantiate a singleton by calling "new" on this function
});